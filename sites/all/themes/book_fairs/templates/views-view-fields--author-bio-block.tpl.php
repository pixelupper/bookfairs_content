<div class="author-bio-block">

<div><?php print $fields['picture']->content;?></div>

<div class="author-bio-names">
	<font class="author-bio-name"><?php print $fields['field_author_name']->content;?></font>
	<font class="author-bio-screen-name"><?php print $fields['field_author_twitter_username']->content;?></font><br>
	<?php print $fields['field_author_bio']->content;?>
</div>

</div>